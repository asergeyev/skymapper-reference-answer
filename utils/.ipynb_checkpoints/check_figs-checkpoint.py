import os
import re
from shutil import copyfile


def main():
	path = '../skymapper/'
	if not os.path.isdir(f'./figs/'):
		os.makedirs(f'./figs/')
	with open(f'{path}main.tex') as fin:
		for line in fin:
			if '.png' in line:
				temp = re.split('{ |}|/', line)
				name = [x for x in temp if '.png' in x][0]
				print(f'{path}figs/{name}')
				if os.path.isfile(f'{path}figs/{name}'):
					copyfile(f'{path}figs/{name}', f'./figs/{name}')
				else:
					print(f'File {name} not found')

			if '.pgf' in line:
				temp = re.split('{ |}|/', line)
				name = [x for x in temp if '.pgf' in x][0]
				print(f'{path}figs/{name}')
				if os.path.isfile(f'{path}figs/{name}'):
					copyfile(f'{path}figs/{name}', f'./figs/{name}')
				else:
					print(f'File {name} not found')


if __name__ == '__main__':
	main()