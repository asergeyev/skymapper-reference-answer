# SkyMapper

## Folders

code\ - contains python code of asterids preprocessing

code\data\ 


## Notes from SkyMag team
- u-i -> red leak!! flux
  - but not the v
  - so a weird u-v color
  - Not a problem for G-like color in principle
- Opened avenues
  - [ ] Just use gr iz
  - [ ] Check u,v colors 
  - [ ] Compare with SDSS
    - How many new
    - Completeness vs H
  - [ ] Purity & Completeness


- We have DR3 xm SkyBot
- DR4 = DR3+ 1.5y
  - That would be July/August
  - Increased volume (~20%)
  - Maybe slightly better u/v ZP
- SkyMapper end
  - Main survey should finish next year (1 exp / filter)
  - Telescope will keep, but different survey
  - DR5 ~ Christmas 2022 
  - The same team would operate
    - Based on "open time" requests

- Check DREAMS-SM 
  - Concurrency on the EC
  - 0.5m - 10s - H~17 5 sigma


