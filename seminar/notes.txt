1. \note{1. Today I will tell you about our last work with the extraction of asteroids from public surveys. It continues our previous work where we extracted Solar system objects from the Sloan Digital Sky Survey, but this time we works with the collaboration of the Australian SkyMapper team and uses their modern data.}

2. \note{OK, SkyMapper. This survey covers the south part of the sky. The observations care out with a relatively small 1.3-meter telescope with a huge almost 6 square degrees field of view. The telescope equipped by two hundred sixty-eight million (268) pixels CCD camera and set of standard optical filters. Skymapper survey covers twenty-one thousand (21,000) square degrees area of the southern part of the sky.}

3. \note{We are used the SkyBot service to predict positions for known asteroids. The Skybot predicted 16 million positions of SSO in the Skymapper field of view. But only 2 million objects were identified in the locations. It because the Skymapper survey i s not so deep, especially in v and u filters. So, most faint asteroids were not visible on the Skymapper images.
We matched SkyMapper objects with the GAIA catalog to exclude stars and other stationary objects, fortunately, SkyMapper and GAIA have a similar magnitude deepness. 
But after it, we noted a cloud of objects with deviated relative to predicted by SkyBot magnitudes. 
We applied several cleaning filters to remove these objects and as result, we obtained a sample of almost a nighty hundred thousand (900,000) observations of two hundred thousand Solar System Objects.
We can see The statistics these objects by dynamical classes.

The completeness of bright objects is about 95 percent and drops on faint objects.
}

4. \note{There are presented a dynamical complexes.

For the bright asteroids, the purity is almost 100 percent and drops after 21 magnitude}

5. \note{In the difference with the Sloan survey, the SkyMapper observations used only one filter for exposure, fortunately the telescope changed filters very fast ,in the shallow regime, after each exposure that  allowed to estimate colors of some of Skymapper asteroids, which have a little time delay between observations in different filters. How you can see on a cumulative histogram almost 90% of observation in g-r and i-z filter pairs have a delay of less than 2 minuts. That allow to estimate colors}

6. \note{Ok, from obtained asteroids colors we estimated a probable taxonomy complex by checking of belonging asteroid colors to color boundaries of different complexes. 
Below you can see spectra templates from Bus Dmeo taxonomy and magnitude reflectance of SkyMapper asteroids for each predicted taxonomy complex.

7. \note{This figure of the Main Belt orbital elements of SkyMapper asteroids illustrate the potential of this taxonomic classification. 
We can clearly see the concentration different asteroids classes by families. For example asteroids of the Vesta family (light green points) concentrated in the inner belt where dominated asteroids of S complex (red points).
C and B complexes dominate in the outer belt region, however, the Kronos family at the 2.9 au distance shows a confident S complex taxonomy. 
Interesting also to note that X-type asteroids (are indicated by gray points) concentrate in the outer belt region associated with the Eos family.}

8. \note{But the biggest fraction of asteroids extracted from Skymapper has information only about g-r color. Here is present the histogram of g-r color distribution where see two normal distributions. We can suppose that red asteroids belong to inner - S-type like asteroids while blues are C-type asteroids. We separated them by fitting this distribution by two Gaussians and classified asteroids by using only g-r color.}

9. \note{And voile, Here the orbital elements are based only on such simple separation of complexes. Of course, we cannot distinguish V and X type asteroids, however, it clearly shows concentrations of taxonomy by families.}